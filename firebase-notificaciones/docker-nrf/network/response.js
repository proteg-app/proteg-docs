/* Respuestas personalizadas para peticiones asíncronas exitosas y fallidas. */

/* Éxito. */
exports.success = function(req, res, message, status){
    console.log(message);
    res.send({
        error: '',
        body: message,
    });
}
/* Fallida. */
exports.error = function(req, res, message, status){
    console.log(message);
    res.send({
        error: message,
        body: '',
    });
}