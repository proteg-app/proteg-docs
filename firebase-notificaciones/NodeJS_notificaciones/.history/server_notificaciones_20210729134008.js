/* Se importa las respuestas personalizadas. */
const response = require('./network/response');
/* Sección donde se importa todo lo relacionado a express. */
const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
/* Se inicializa express. */
const app = express();
/* Configuración del servidor. */
const PUERTO = 8080;
const HOST = '0.0.0.0';

/* Se añade el router y body-parser a la aplicación de express. */
app.use(bodyParser.urlencoded({extended: false}));
app.use(router);

/* Sección donde se importa todo lo relacionado a Firebase. */
/* Importo la llave privada de Firebase. */
const admin = require("firebase-admin");
const serviceAccount = require("./key/license.json");
/* Inicializo el servicio utilizando la licencia que viene como JSON. */
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://reactnative-evotek-tests.firebaseio.com"
});

/* Se define la ruta de polizas para la API. */
 ('/notificaciones/polizas', function(req, res){
let datos = JSON.parse(req.query.datos);
/* Se destructura la información. */
let { tokenCelular, titulo, mensaje } = req.query;
/* Valido que los campos no vengan vacíos. */
if(!tokenCelular || !titulo || !mensaje || !datos){
  res.status(500).send("Intenta de nuevo más tarde... (34)");
  return false;
}
/* Se crea el payload con el título y el mensaje para la notificación.*/
let payload = {
  notification: {
    title: titulo,
    body: mensaje,
  },
  data: {
      id: datos['id'],
      fecha: datos['fecha'],
      num_poliza: datos['num_poliza'],
      archivo_poliza: datos['archivo_poliza'],
      aseguradora_nombre: datos['aseguradora_nombre'],
      seguro_nombre: datos['seguro_nombre'],
      activo: datos['activo'],
      seguro_descripcion: datos['seguro_descripcion'],
      clase: datos['clase'],
      beneficiario: datos['beneficiario'],
  }
};
/* Opciones de configuración. */
let opciones = {
  priority: "normal",
  timeToLive: 60 * 60
};
// console.log(payload);
/* Proceso para intentar enviar una notificación al dispositivo al que está vinculado el token. */
admin.messaging().sendToDevice(tokenCelular, payload, opciones)
.then(function(firebaseResponse) {
  /* Si todo salió bien regresaremos 200. */
  if(firebaseResponse.failureCount == 0 && firebaseResponse.successCount == 1){
    response.success(req, res, '¡Se ha enviado correctamente la notificación!', 200);
  }else{
    response.error(req, res, 'Ha ocurrido un error, intenta de nuevo más tarde... (69)', 500);
  }
})
.catch(function(error) {
  response.error(req, res, `Ha ocurrido un error, intenta de nuevo más tarde... (73)${error}`, 500);
});
});

/* Se define la ruta de pagos para la API. */
 ('/notificaciones/pagos', function(req, res){
  /* Se destructura la información. */
  let { tokenCelular, titulo, mensaje } = req.query;
  /* Valido que los campos no vengan vacíos. */
  if(!tokenCelular || !titulo || !mensaje){
    res.status(500).send("Intenta de nuevo más tarde... (83)");
    return false;
  }
  /* Se crea el payload con el título y el mensaje para la notificación.*/
  let payload = {
    notification: {
      title: titulo,
      body: mensaje,
    },
  };
  /* Opciones de configuración. */
  let opciones = {
    priority: "normal",
    timeToLive: 60 * 60
  };

  console.log('-'*50)
  console.log('payload');
  console.log(payload);
  console.log('-'*50)
  console.log('token');
  console.log(tokenCelular);
  console.log('-'*50)
  /* Proceso para intentar enviar una notificación al dispositivo al que está vinculado el token. */
  admin.messaging().sendToDevice(tokenCelular, payload, opciones)
  .then(function(firebaseResponse) {
    /* Si todo salió bien regresaremos 200. */

    // console.log('*'*30+' tokenCelular'+'*'*30);
    // console.log(tokenCelular);
    // console.log('*'*60);
    // console.log('*'*30+' payload'+'*'*30);
    // console.log(payload);
    // console.log('*'*60);
    // console.log('*'*30+' opciones'+'*'*30);
    // console.log(opciones);
    // console.log('*'*60);
    
    if(firebaseResponse.failureCount == 0 && firebaseResponse.successCount == 1){
      // console.log('entro')
      response.success(req, res, '¡Se ha enviado correctamente la notificación!', 200);
    }else{
      // console.log(firebaseResponse.results[0].error);
      response.error(req, res, 'Ha ocurrido un error, intenta de nuevo más tarde... (106)', 500);
    }
  })
  .catch(function(error) {
    // console.log('ya no');
    // console.log(error);
    response.error(req, res, 'Ha ocurrido un error, intenta de nuevo más tarde... (111)', 500);
  });
  });

/* Se monta el servidor en el puerto 8080. */
app.listen(PUERTO);
console.log(`Contenedor corriendo en http://${HOST}:${PUERTO}`);
// console.log('tests...')

// let payload = {
//   notification: {
//     title: 'prueba',
//     body: 'probacion',
//   },
// };
// /* Opciones de configuración. */
// let opciones = {
//   priority: "normal",
//   timeToLive: 60 * 60
// };

// admin.messaging().sendToDevice('curoP1j0S6mI59yrC1tbz_:APA91bGVK9dCnXAOBM0fXQ26AeY5uLG1RS992kG8ciKA7N9Yse1KAm3sLg7v3_JxtQzvaWQNoatMfldm9PcCmp4kHsl-HPh23f6jAWIxFFdJvEJxBnGvOoEkeHRdqAeuwtWPMotUAVQM', payload, opciones)
// .then(function(firebaseResponse) {
//   /* Si todo salió bien regresaremos 200. */
//   console.log('todo salio bien');
//   if(firebaseResponse.failureCount == 0 && firebaseResponse.successCount == 1){
//     console.log('entro')
//   }else{
//     console.log(firebaseResponse.results[0].error);
//   }
// })
// .catch(function(error) {
//   console.log('ya no');
//   console.log(error);
// });