/* Se importa las respuestas personalizadas. */
const response = require('./network/response');
/* Sección donde se importa todo lo relacionado a express. */
const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const mysql = require('mysql');
/* Se inicializa express. */
const app = express();
/* Configuración del servidor. */
const PUERTO = 8080;
const HOST = '0.0.0.0';

/* Se añade el router y body-parser a la aplicación de express. */
app.use(bodyParser.urlencoded({extended: false}));
app.use(router);

/* Sección donde se importa todo lo relacionado a Firebase. */
/* Importo la llave privada de Firebase. */
const admin = require("firebase-admin");
const serviceAccount = require("./key/license.json");
/* Inicializo el servicio utilizando la licencia que viene como JSON. */
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://reactnative-evotek-tests.firebaseio.com"
});

/* Se define la ruta de polizas para la API. */
router.post('/notificaciones/polizas', function(req, res){
let { tokenCelular, titulo, mensaje } = req.query;
if(!tokenCelular || !titulo || !mensaje){
  res.status(500).send("Intenta de nuevo más tarde... (83)");
  return false;
}
/* Se crea el payload con el título y el mensaje para la notificación.*/
let payload = {
  notification: {
    title: titulo,
    body: mensaje,
  }
};
/* Opciones de configuración. */
let opciones = {
  priority: "normal",
  timeToLive: 60 * 60
};
console.log(payload);
/* Proceso para intentar enviar una notificación al dispositivo al que está vinculado el token. */
admin.messaging().sendToDevice(tokenCelular, payload, opciones)
.then(function(firebaseResponse) {
  /* Si todo salió bien regresaremos 200. */
  if(firebaseResponse.failureCount == 0 && firebaseResponse.successCount == 1){
    response.success(req, res, '¡Se ha enviado correctamente la notificación!', 200);
  }else{
    response.error(req, res, 'Ha ocurrido un error, intenta de nuevo más tarde... (69)', 500);
  }
})
.catch(function(error) {
  response.error(req, res, `Ha ocurrido un error, intenta de nuevo más tarde... (73)${error}`, 500);
});
});

/* Se define la ruta de pagos para la API. */
router.post('/notificaciones/pagos', function(req, res){
  /* Se destructura la información. */
  let { tokenCelular, titulo, mensaje } = req.query;
  /* Valido que los campos no vengan vacíos. */
  if(!tokenCelular || !titulo || !mensaje){
    res.status(500).send("Intenta de nuevo más tarde... (83)");
    return false;
  }
  /* Se crea el payload con el título y el mensaje para la notificación.*/
  let payload = {
    notification: {
      title: titulo,
      body: mensaje,
    },
  };
  /* Opciones de configuración. */
  let opciones = {
    priority: "normal",
    timeToLive: 60 * 60
  };

  console.log('-'*50)
  console.log('payload');
  console.log(payload);
  console.log('-'*50)
  console.log('token');
  console.log(tokenCelular);
  console.log('-'*50)
  /* Proceso para intentar enviar una notificación al dispositivo al que está vinculado el token. */
  admin.messaging().sendToDevice(tokenCelular, payload, opciones)
  .then(function(firebaseResponse) {
    /* Si todo salió bien regresaremos 200. */

    console.log('*'*30+' tokenCelular'+'*'*30);
    console.log(tokenCelular);
    console.log('*'*60);
    console.log('*'*30+' payload'+'*'*30);
    console.log(payload);
    console.log('*'*60);
    console.log('*'*30+' opciones'+'*'*30);
    console.log(opciones);
    console.log('*'*60);
    
    if(firebaseResponse.failureCount == 0 && firebaseResponse.successCount == 1){
      console.log('entro')
      response.success(req, res, '¡Se ha enviado correctamente la notificación!', 200);
    }else{
      console.log(firebaseResponse.results[0].error);
      response.error(req, res, 'Ha ocurrido un error, intenta de nuevo más tarde... (106)', 500);
    }
  })
  .catch(function(error) {
    console.log('ya no');
    console.log(error);
    response.error(req, res, 'Ha ocurrido un error, intenta de nuevo más tarde... (111)', 500);
  });
});

/* Se define la ruta para cuando se agerga una publicidad . */
router.post('/notificaciones/publicidad', function(req, res){
  /* Se crea el payload con el título y el mensaje para la notificación.*/
  let { titulo, descripcion } = req.query;
  /* Valido que los campos no vengan vacíos. */
  if(!titulo || !descripcion){
    res.status(500).send("Intenta de nuevo más tarde... (124)");
    return false;
  }
  let payload = {
    notification: {
      title: titulo,
      body: descripcion,
    },
  };
  
  /* Opciones de configuración. */
  let opciones = {
    priority: "normal",
    timeToLive: 60 * 60
  };

  const conexion = mysql.createConnection({
    host: "localhost",
    user: "garechav",
    password: "garechav",
    database: "proteg",
});
/* Se crea una conexión a la base de datos. */
  conexion.connect(function(err) {
      /* Si hay un error lo mostraremos y pararemos todo. */
      if(err){
          console.log(err);
      }else{
          /* Consulta. */
          const consulta = `
          SELECT distinct cliente.token_id
          FROM cliente
          WHERE cliente.token_id is not null and cliente.activo=1;`;
          /* Si no se procederá a realizar una consulta a la base de datos. */
          conexion.query(consulta, function (err, result, fields) {
              /* Cierro la conexión. */
              conexion.end();
              /* Si hay un error lo mostraremos y pararemos todo. */
              if(err) console.log(err);
              /* Si sí hay un resultado en la consulta procederemos a realizar el proceso de envío de notificaciones.*/
              if(result){
                result.forEach(cliente => {
                  /* Si el pago tiene un cliente con un token validado se entrará al siguiente condicional. */
                  admin.messaging().sendToDevice(cliente.token_id, payload, opciones)
                  .then(function(firebaseResponse) {
                    /* Si todo salió bien regresaremos 200. */
                    if(firebaseResponse.failureCount == 0 && firebaseResponse.successCount == 1){
                      console.log('entro')
                      response.success(req, res, '¡Se ha enviado correctamente la notificación!', 200);
                    }else{
                      console.log(firebaseResponse.results[0].error);
                      response.error(req, res, 'Ha ocurrido un error, intenta de nuevo más tarde... (169)', 500);
                    }
                  })
                  .catch(function(error) {
                    console.log('ya no');
                    console.log(error);
                    response.error(req, res, 'Ha ocurrido un error, intenta de nuevo más tarde... (175)', 500);
                  });
                });
              }else{
                  console.log('No hubo pagos: ' + new Date());
              }
          });
      }
  });

});

/* Se monta el servidor en el puerto 8080. */
app.listen(PUERTO);
console.log(`Contenedor corriendo en http://${HOST}:${PUERTO}`);
// console.log('tests...')

// let payload = {
//   notification: {
//     title: 'prueba',
//     body: 'probacion',
//   },
// };
// /* Opciones de configuración. */
// let opciones = {
//   priority: "normal",
//   timeToLive: 60 * 60
// };

// admin.messaging().sendToDevice('curoP1j0S6mI59yrC1tbz_:APA91bGVK9dCnXAOBM0fXQ26AeY5uLG1RS992kG8ciKA7N9Yse1KAm3sLg7v3_JxtQzvaWQNoatMfldm9PcCmp4kHsl-HPh23f6jAWIxFFdJvEJxBnGvOoEkeHRdqAeuwtWPMotUAVQM', payload, opciones)
// .then(function(firebaseResponse) {
//   /* Si todo salió bien regresaremos 200. */
//   console.log('todo salio bien');
//   if(firebaseResponse.failureCount == 0 && firebaseResponse.successCount == 1){
//     console.log('entro')
//   }else{
//     console.log(firebaseResponse.results[0].error);
//   }
// })
// .catch(function(error) {
//   console.log('ya no');
//   console.log(error);
// });