/* Exportamos cron, express, axios y mysql. */
const cron = require("node-cron");
const express = require("express");
const mysql = require('mysql');
const axios = require('axios')

/* Instancia del servidor de express. */
app = express();

/* Todos los días a las 12 en punto se abrirá una conexión a MySQL que buscará los pagos
que están a punto de vencer y le enviará una notificación a los usuarios de que sus pagos están proximos a vencer. */
cron.schedule("35 12 * * *", function() {
    /* Configuración de la conexión a la base de datos.*/
    const conexion = mysql.createConnection({
        host: "localhost",
        user: "garechav",
        password: "garechav",
        database: "proteg",
    });
    /* Se crea una conexión a la base de datos. */
    conexion.connect(function(err) {
        /* Si hay un error lo mostraremos y pararemos todo. */
        if(err){
            console.log(err);
        }else{
            /* Consulta. */
            const consulta = `
            SELECT cliente.id, cliente.token_id, cliente.nombre, poliza.num_poliza, pago.fecha, pago.fecha_venci
            FROM pago LEFT JOIN poliza ON poliza.id = pago.id_poliza 
            LEFT JOIN cliente ON cliente.id = poliza.interesado_id_cliente
            WHERE pago.id_status = 1 
            AND DATE(pago.fecha_venci) BETWEEN (CURDATE() - INTERVAL 30 DAY) AND CURDATE();`;
            /* Si no se procederá a realizar una consulta a la base de datos. */
            conexion.query(consulta, function (err, result, fields) {
                /* Cierro la conexión. */
                conexion.end();
                /* Si hay un error lo mostraremos y pararemos todo. */
                if(err) console.log(err);
                /* Si sí hay un resultado en la consulta procederemos a realizar el proceso de envío de notificaciones.*/
                if(result){
                    /* Se procederá a guardar en un arreglo la información del cliente y el total de pagos que tiene por vencer. */
                    let clientes = [];
                    /* Iteraremos a través de cada elemento de la consulta. */
                    result.forEach(cliente => {
                        /* Si el pago tiene un cliente con un token validado se entrará al siguiente condicional. */
                        if(cliente.token_id){
                            clientes.push(cliente.token_id+'@nombre:'+cliente.nombre);
                        }
                    });
                    /* Se eliminarán duplicados y sólo se dejarán los tokens únicos. */
                    let unico = clientes.filter(function(value, index, self) { 
                        return self.indexOf(value) === index;
                    });
                    /* Se iterará a través de cada cliente único (basados en el token) para enviar via POST los parámetros 
                    al otro contenedor docker y así poder enviar una notificación. */
                    if(unico){
                        unico.forEach(element => {
                            let data = element.split("@nombre:");
                            let token = data[0];
                            let titulo = encodeURI(`¡Hola ${data[1]}!`);
                            /* Se hará una petición via POST a la API de notificaciones. */
                            axios.post(`http://198.12.248.204:8080/notificaciones/pagos?tokenCelular=${token}&titulo=${titulo}&mensaje=Tienes%20pagos%20pr%C3%B3ximos%20a%20vencer.%20Revisa%20tus%20cuentas`)
                            .then(response => {
                                let informacion_consola = {
                                    'token' : token,
                                    'nombre' : data[1],
                                    'status' : 200,
                                    'fecha' : new Date(),
                                }
                                console.log('_________________________________');
                                console.log(response.data);
                                console.log(informacion_consola);
                                console.log('---------------------------------');
                            })
                            .catch(error => {
                                console.log(error);
                            });
                        });
                    }else{
                        console.log('No hubo pagos: ' + new Date());
                    }  
                }else{
                    console.log('No hubo pagos: ' + new Date());
                }
            });
        }
    });
});

/* Configuración del servidor. */
let puerto = 8081;
console.log('[Aplicación corriendo en el puerto: ' + puerto + ']');
app.listen(puerto);
