CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `seguro_archivos`
--

DROP TABLE IF EXISTS `seguro_archivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seguro_archivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_seguro_tipo` int(11) NOT NULL,
  `id_tipo_archivo` int(11) NOT NULL,
  `id_tipo_persona` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seguro_archivos`
--

LOCK TABLES `seguro_archivos` WRITE;
/*!40000 ALTER TABLE `seguro_archivos` DISABLE KEYS */;
INSERT INTO `seguro_archivos` VALUES (1,1,1,1),(2,1,2,1),(3,1,3,1),(4,1,4,2),(6,2,1,2),(7,2,2,2),(8,2,3,1),(9,2,4,1),(10,2,5,1),(11,3,1,1),(12,3,2,1),(13,3,3,1),(14,3,4,1),(15,3,5,1),(16,4,1,1),(17,4,2,1),(18,4,3,1),(19,4,4,1),(20,4,5,1),(21,5,1,1),(22,5,2,1),(23,5,3,1),(24,5,4,1),(25,5,5,1),(26,6,1,1),(27,6,2,1),(28,6,3,1),(29,6,4,1),(30,6,5,1),(31,7,1,1),(32,7,2,1),(33,7,3,1),(34,7,4,1),(35,7,5,1),(36,8,1,1),(37,8,2,1),(38,8,3,1),(39,8,4,1),(40,8,5,1),(41,9,1,1),(42,9,2,1),(43,9,3,1),(44,9,4,1),(45,9,5,1);
/*!40000 ALTER TABLE `seguro_archivos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:26:37
