CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `siniestro`
--

DROP TABLE IF EXISTS `siniestro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `siniestro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_poliza` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `id_quien_atiende` int(11) DEFAULT NULL,
  `quien_reporta` varchar(100) DEFAULT NULL,
  `activo` int(4) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  `archivo` varchar(250) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `ubicacion` varchar(100) DEFAULT NULL,
  `referencia` varchar(250) DEFAULT NULL,
  `lugar` varchar(250) DEFAULT NULL,
  `fecha_captura` datetime DEFAULT NULL,
  `folio` varchar(300) DEFAULT NULL,
  `tajo` varchar(300) DEFAULT NULL,
  `nombre_reporte` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siniestro`
--

LOCK TABLES `siniestro` WRITE;
/*!40000 ALTER TABLE `siniestro` DISABLE KEYS */;
INSERT INTO `siniestro` VALUES (1,1,'2020-10-02 00:00:00',6,'DAVID ADAME',1,2,NULL,'daños materiales',NULL,NULL,'juarez e hidalgo','2020-10-02 14:02:43','777','chocó con un poste','choque con poste'),(2,2,'2020-11-06 00:00:00',2,'pruebaaaa',1,3,NULL,'preba',NULL,NULL,'prueba','2020-11-06 10:53:28','prebaa1','prueba','prueba'),(3,25,'2021-05-27 00:00:00',4,'El alcalde de torreon coahuila',1,2,NULL,'descripcion',NULL,NULL,'torreon coahuila','2021-05-27 20:25:31','00001','siniestro regular','Revisen este siniestro');
/*!40000 ALTER TABLE `siniestro` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:27:04
