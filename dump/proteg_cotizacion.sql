CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cotizacion`
--

DROP TABLE IF EXISTS `cotizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cotizacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` varchar(45) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `id_empleado_vendedor` int(11) DEFAULT NULL,
  `id_seguro_tipo` int(11) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `interesado_id_cliente` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT '1',
  `activo` int(4) DEFAULT NULL,
  `archivo` varchar(250) DEFAULT NULL,
  `cotizacion` varchar(250) DEFAULT NULL,
  `id_aseguradora` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cotizacion`
--

LOCK TABLES `cotizacion` WRITE;
/*!40000 ALTER TABLE `cotizacion` DISABLE KEYS */;
INSERT INTO `cotizacion` VALUES (1,'cotizacion-prueba','2020-10-01 00:00:00',4,1,'Cotización prueba...  2',1,2,1,'../../cotizaciones/cotizacion-prueba/informacion-minima-2116244982-exemplo.xls','../../cotizaciones/cotizacion-prueba/cotizacion-1906621246-sample.pdf',1),(2,'123-2020','2020-10-02 00:00:00',4,1,'SEGURO PARA MURANO 2009',1,2,1,'../../cotizaciones/123-2020/informacion-minima-1819772112-Relacion Quejas MAC 2020.xlsx','../../cotizaciones/123-2020/1157449412-Cotización.pdf',2),(3,'123','2020-10-02 00:00:00',6,1,'seguro para camioneta',1,3,1,'../../cotizaciones/123/informacion-minima-1368662554-Relacion Quejas MAC 2020.xlsx','../../cotizaciones/123/cotizacion-1531373464-Cotización.pdf',NULL),(4,'prueba1','2020-11-04 00:00:00',2,3,'prueba1',2,3,1,'','',NULL),(5,'prueba2','2020-11-05 00:00:00',2,1,'prueba2',1,1,1,'','',NULL),(6,'prueba3','2020-11-03 00:00:00',2,5,'prueba3',2,2,1,'','',6),(7,'2561','2021-05-26 00:00:00',4,1,'demostracion001',1,1,1,'../../cotizaciones/2561/informacion-minima-186974976-Libro 4.xlsx','../../cotizaciones/2561/cotizacion-927285728-pdf.pdf',NULL),(8,'005','2021-05-31 00:00:00',4,1,'prueba',17,1,1,'../../cotizaciones/005/informacion-minima-1041735969-Libro 4.xlsx','../../cotizaciones/005/cotizacion-1186220507-pdf.pdf',NULL);
/*!40000 ALTER TABLE `cotizacion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:26:52
