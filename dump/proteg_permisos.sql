CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `permisos`
--

DROP TABLE IF EXISTS `permisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pagina` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permisos`
--

LOCK TABLES `permisos` WRITE;
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` VALUES (1,7,4),(2,1,1),(4,3,1),(5,4,1),(6,5,1),(7,6,1),(9,8,1),(10,7,1),(11,10,1),(12,12,1),(13,13,1),(14,14,1),(15,15,1),(16,16,1),(17,17,1),(18,1,2),(19,3,2),(20,2,2),(21,4,2),(22,5,2),(23,6,2),(24,7,2),(25,8,2),(26,10,2),(27,12,2),(28,13,2),(29,14,2),(30,15,2),(31,17,2),(32,16,2),(33,1,3),(34,2,3),(35,3,3),(36,4,3),(37,5,3),(38,6,3),(39,7,3),(40,8,3),(41,10,3),(43,12,3),(44,13,3),(45,14,3),(46,15,3),(47,16,3),(48,17,3),(49,1,4),(50,2,4),(51,3,4),(52,4,4),(53,5,4),(54,6,4),(56,8,4),(57,10,4),(58,12,4),(59,13,4),(60,14,4),(61,15,4),(62,16,4),(63,17,4),(64,1,5),(65,2,5),(66,3,5),(67,5,5),(68,4,5),(69,6,5),(70,7,5),(71,8,5),(72,10,5),(73,12,5),(74,13,5),(75,14,5),(76,15,5),(77,16,5),(78,17,5),(83,3,6),(84,4,6),(85,5,6),(86,6,6),(87,7,6),(88,8,6),(90,10,6),(92,12,6),(93,13,6),(94,15,6),(95,14,6),(96,16,6),(97,17,6),(99,1,7),(100,2,7),(101,3,7),(102,4,7),(103,5,7),(104,6,7),(105,7,7),(106,8,7),(108,10,7),(109,12,7),(110,13,7),(111,14,7),(112,15,7),(113,17,7),(114,16,7),(116,2,6),(117,1,6),(119,18,6),(120,20,6),(122,20,4),(123,20,2),(124,18,2),(125,9,4),(126,11,4),(127,18,4);
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:26:45
