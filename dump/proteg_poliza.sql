CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `poliza`
--

DROP TABLE IF EXISTS `poliza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `poliza` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cotizacion` int(11) DEFAULT NULL,
  `num_poliza` varchar(45) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `id_empleado_vendedor` int(11) DEFAULT NULL,
  `id_seguro_tipo` int(11) DEFAULT NULL,
  `interesado_id_cliente` int(11) DEFAULT NULL,
  `activo` int(4) DEFAULT NULL,
  `archivo_poliza` varchar(250) DEFAULT NULL,
  `fecha_inicial` datetime DEFAULT NULL,
  `id_aseguradora` int(11) DEFAULT NULL,
  `fecha_final` datetime DEFAULT NULL,
  `archivo_solicitud` varchar(250) DEFAULT NULL,
  `poliza_padre` int(4) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `beneficiario` varchar(200) DEFAULT NULL,
  `estatus` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poliza`
--

LOCK TABLES `poliza` WRITE;
/*!40000 ALTER TABLE `poliza` DISABLE KEYS */;
INSERT INTO `poliza` VALUES (1,1,'P1234','2020-10-02 11:47:23',4,1,1,1,'../../polizas/P1234/poliza_P1234.pdf','2020-10-02 00:00:00',2,'2021-10-02 00:00:00','',0,'POLIZA DE MURANO 2009','Gabriel Francisco De Leon Jr.','1'),(2,1,'P4567','2020-10-02 13:31:41',6,1,1,1,'../../polizas/P4567/poliza_P4567.pdf','2020-10-02 00:00:00',2,'2021-10-02 00:00:00','../../polizas/P4567/solicitud_P4567.pdf',0,'poliza de camioneta para gabriel','Gabriel Francisco De Leon Rumayor','1'),(3,6,'prueba-poli','2020-11-05 14:15:16',2,5,2,1,'','2020-11-06 00:00:00',6,'2020-11-08 00:00:00','',0,'prueba póliza','JALO TRANSPORTES S.A. DE C.V.','3'),(4,6,'prueba3','2020-11-05 14:25:19',2,5,2,1,'','2020-11-06 00:00:00',6,'2020-11-08 00:00:00','',0,'prueba','JALO TRANSPORTES S.A. DE C.V.','3'),(5,1,'P4567','2020-11-06 10:44:32',2,1,1,1,'../../polizas/P4567/poliza-591915109-taza de cafe final.pdf','2020-10-02 00:00:00',2,'2021-10-02 00:00:00','',2,'pruebaaj','pruebadfghj','3'),(6,1,'test','2020-12-21 02:57:21',4,1,1,1,'','2020-12-01 00:00:00',1,'2020-12-31 00:00:00','',0,'test','Gabriel Francisco De Leon Rumayor','1'),(7,1,'123412421','2020-12-21 03:02:38',4,1,1,1,'','2020-11-30 00:00:00',1,'2020-12-21 00:00:00','',0,'421421','Gabriel Francisco De Leon Rumayor','1'),(8,1,'142421','2020-12-21 03:07:37',4,1,1,1,'','2020-12-01 00:00:00',1,'2020-12-30 00:00:00','',0,'124421','Gabriel Francisco De Leon Rumayor','1'),(9,1,'421421421','2020-12-21 03:16:39',4,1,1,1,'','2020-12-01 00:00:00',1,'2020-12-31 00:00:00','',0,'421421','Gabriel Francisco De Leon Rumayor','1'),(10,1,'testetst','2020-12-21 03:22:11',4,1,1,1,'','2020-12-01 00:00:00',1,'2020-12-23 00:00:00','',0,'testesst','Gabriel Francisco De Leon Rumayor','1'),(11,1,'gabrieltest','2020-12-27 22:17:31',4,1,1,1,'','2020-12-02 00:00:00',1,'2020-12-31 00:00:00','',0,'34232432','Gabriel Francisco De Leon Rumayor','1'),(12,1,'421421','2020-12-27 22:28:24',4,1,1,1,'','2020-12-09 00:00:00',1,'2020-12-29 00:00:00','',0,'42124','Gabriel Francisco De Leon Rumayor','1'),(13,1,'123213rewrew','2020-12-27 23:27:02',4,1,1,1,'','2020-12-27 00:00:00',1,'2020-12-30 00:00:00','',0,'32132132123','Gabriel Francisco De Leon Rumayor','1'),(14,2,'44fdsgfd','2020-12-27 23:27:34',4,1,1,1,'','2020-12-03 00:00:00',2,'2020-12-30 00:00:00','',0,'421421dfsfdsfd','Gabriel Francisco De Leon Rumayor','1'),(15,1,'123abzcxv','2020-12-27 23:29:30',4,1,1,1,'','2020-12-02 00:00:00',1,'2020-12-24 00:00:00','',0,'fdgfdsgfd','Gabriel Francisco De Leon Rumayor','1'),(16,1,'123abcabc','2020-12-27 23:50:45',4,1,1,1,'','2020-12-01 00:00:00',1,'2020-12-31 00:00:00','',0,'1234','Gabriel Francisco De Leon Rumayor','1'),(17,2,'421421421421fdsfdsdsa','2020-12-27 23:55:09',4,1,1,1,'','2020-12-04 00:00:00',2,'2020-12-15 00:00:00','',0,'4321421421','Gabriel Francisco De Leon Rumayor','1'),(18,2,'421421421421','2020-12-27 23:55:37',4,1,1,1,'','2020-12-04 00:00:00',2,'2020-12-25 00:00:00','',0,'421421421','Gabriel Francisco De Leon Rumayor','1'),(19,1,'123abcasdasdadsa','2020-12-27 23:57:53',4,1,1,1,'','2020-12-03 00:00:00',1,'2020-12-15 00:00:00','',0,'214421421','Gabriel Francisco De Leon Rumayor','1'),(20,1,'cxvczvc','2021-01-03 19:14:28',4,1,1,1,'','2021-01-01 00:00:00',1,'2021-01-26 00:00:00','',0,'fdssdf','Gabriel Francisco De Leon Rumayor','1'),(21,2,'213vcxzcvcvxz','2021-01-03 19:21:49',4,7,1,1,'','2021-01-01 00:00:00',2,'2021-01-21 00:00:00','',0,'2132132fds','Gabriel Francisco De Leon Rumayor','1'),(22,1,'123412421','2021-01-03 21:08:52',4,1,16,1,'../../polizas/123412421/poliza-adjunta-1901766619-sample.pdf','2020-11-30 00:00:00',1,'2020-12-21 00:00:00','',7,'Esto es una prueba :)','jesus','1'),(23,1,'231321321321321321','2021-01-04 05:31:19',4,1,1,1,'','2021-01-27 00:00:00',1,'2021-01-21 00:00:00','',0,'213321321321','Gabriel Francisco De Leon Rumayor','1'),(24,2,'213213','2021-01-12 01:33:58',4,1,1,1,'','2021-01-01 00:00:00',2,'2021-01-27 00:00:00','',0,'42142121','Gabriel Francisco De Leon Rumayor','1'),(25,1,'255','2021-05-27 20:19:03',4,1,1,1,'','2021-05-27 00:00:00',1,'2021-05-27 00:00:00','',0,'otra prueba','Gabriel Francisco De Leon Rumayor','1'),(26,NULL,'P12344','2021-05-27 20:23:25',4,NULL,1,1,'',NULL,NULL,NULL,'',NULL,'desc','prueba','1'),(27,1,' GM0000098590','2021-06-04 11:41:33',4,9,1,1,'../../polizas/ GM0000098590/poliza_ GM0000098590.pdf','2021-06-01 00:00:00',16,'2022-06-01 00:00:00','',0,'PÓLIZA GASTOS MÉDICOS MAYORES ','Gabriel Francisco De Leon Rumayor','1');
/*!40000 ALTER TABLE `poliza` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:26:55
