CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bitacora_cotizacion`
--

DROP TABLE IF EXISTS `bitacora_cotizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bitacora_cotizacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_cotizacion` int(4) DEFAULT NULL,
  `folio` varchar(45) NOT NULL,
  `id_status` int(11) NOT NULL,
  `descripcion` varchar(3000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitacora_cotizacion`
--

LOCK TABLES `bitacora_cotizacion` WRITE;
/*!40000 ALTER TABLE `bitacora_cotizacion` DISABLE KEYS */;
INSERT INTO `bitacora_cotizacion` VALUES (1,'2020-10-02 10:03:26',4,1,'cotizacion-prueba',1,'Nallely Jaquelin Puente Carranza ha registrado una cotización con el folio: cotizacion-prueba.'),(2,'2020-10-02 11:19:44',4,2,'123-2020',1,'Nallely Jaquelin Puente Carranza ha registrado una cotización con el folio: 123-2020.'),(3,'2020-10-02 11:43:11',4,2,'123-2020',2,'Nallely Jaquelin Puente Carranza vendió una cotización con el folio: 123-2020.'),(4,'2020-10-02 13:05:29',6,1,'cotizacion-prueba',2,'Jesús Ángel Saucedo Ruiz vendió una cotización con el folio: cotizacion-prueba.'),(5,'2020-10-02 13:19:01',NULL,1,'cotizacion-prueba',2,' vendió una cotización con el folio: cotizacion-prueba.'),(6,'2020-10-02 13:22:59',6,3,'123',1,'Jesús Ángel Saucedo Ruiz ha registrado una cotización con el folio: 123.'),(7,'2020-10-02 13:24:42',NULL,3,'123',3,' no logró concretar la venta de la cotización con folio: 123.'),(8,'2020-11-05 13:33:58',2,4,'prueba1',1,'Marcela Salazar Sandoval ha registrado una cotización con el folio: prueba1.'),(9,'2020-11-05 13:43:54',2,4,'prueba1',3,'Marcela Salazar Sandoval no logró concretar la venta de la cotización con folio: prueba1 por el motivo: prueba.'),(10,'2020-11-05 14:08:26',2,5,'prueba2',1,'Marcela Salazar Sandoval ha registrado una cotización con el folio: prueba2.'),(11,'2020-11-05 14:12:31',2,6,'prueba3',1,'Marcela Salazar Sandoval ha registrado una cotización con el folio: prueba3.'),(12,'2020-11-05 14:14:08',2,6,'prueba3',2,'Marcela Salazar Sandoval vendió una cotización con el folio: prueba3.'),(13,'2021-05-27 20:21:52',4,7,'2561',1,'Nallely Jaquelin Puente Carranza ha registrado una cotización con el folio: 2561.'),(14,'2021-05-31 16:04:35',4,8,'005',1,'Nallely Jaquelin Puente Carranza ha registrado una cotización con el folio: 005.');
/*!40000 ALTER TABLE `bitacora_cotizacion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:26:47
