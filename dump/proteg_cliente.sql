CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(45) DEFAULT NULL,
  `id_tipo_persona` int(11) DEFAULT NULL,
  `nombre` varchar(500) DEFAULT NULL,
  `rfc` varchar(45) DEFAULT NULL,
  `direccion` varchar(500) DEFAULT NULL,
  `ciudad` varchar(500) DEFAULT NULL,
  `id_estado` int(11) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `tel` varchar(100) DEFAULT NULL,
  `cel` varchar(100) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `contacto` varchar(500) DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `pwd` varchar(100) DEFAULT NULL,
  `activo` int(11) DEFAULT '1',
  `id_perfil` int(11) DEFAULT NULL,
  `estatus_tutorial` varchar(45) DEFAULT NULL,
  `token_id` varchar(800) DEFAULT NULL,
  `tokenId` varchar(900) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'0001',1,'Gabriel Francisco De Leon Rumayor','LERG790908J1A','Huitzilopochtli 771  Rdcial Los pinos.','Saltillo',5,25240,'528441627707','528441627707','g.deleon@proteg.mx','Gabriel Francisco De Leon Rumayor.','0001','0b4edf7695e1248e0703bf695c44498d',1,4,'1','dkjRut-VQhuHX54Yw4UyPN:APA91bEsD_fCc8HXCM9JaEVxSaH4RxdbX_ySOqZqz8RfxaO-sAvOeBoR7z1_Ube1Az4Vr4a89Tv2fMKxzmEs4H1UdMLERADFpxVK3KIFane7k56fqgx6P-WglHmmZ_GgVtD2IyRArvS9',NULL),(2,'0002',2,'JALO TRANSPORTES S.A. DE C.V.','1','LIB OSCAR FLORES TAPIA KM 12 COL. RURAL AG OTE','COAHUILA',NULL,25016,'8444897872','8444897872','jlopez@jalo.com.mx','Jorge A. Lopez Gutierrez','0002','123456',1,4,'1','',NULL),(16,'jesus',4,'Jesús','1234567890','jesus','jesus',5,NULL,'86612345566','3218471428','jesus@jesus.com',NULL,'jesus','110d46fcd978c24f306cd7fa23464d73',1,4,'1','e6TSL5JkB_M:APA91bF7gWOfClb4wy__ij78x6jG2t5ufixmx1BY34cK7Tf8tAC7WeHfHNr4CLxNOw1FITQBYKf70P88ZhrX4Vfc0FVVyGmjeYc1ALyEOlL18ExHvqkdJLyBNQRzebvKHsIEdbXNEPg5',NULL),(17,'8888888888',2,'Ivan Medina','AAAA07158947Q','escuela','saltillo',5,NULL,'8888888888','8888888888','ivan@ivan.com',NULL,'ivan','0339df446b5ab1307d178b3f87a3d0fd',1,4,NULL,NULL,NULL);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:26:13
