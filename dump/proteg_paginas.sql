CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `paginas`
--

DROP TABLE IF EXISTS `paginas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paginas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagina` varchar(500) DEFAULT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `url_pagina` varchar(500) DEFAULT NULL,
  `icono_pagina` varchar(500) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paginas`
--

LOCK TABLES `paginas` WRITE;
/*!40000 ALTER TABLE `paginas` DISABLE KEYS */;
INSERT INTO `paginas` VALUES (1,'Registrar cotización',2,'../cotizaciones/registrar_cotizacion.php','add',1),(2,'Ver cotizaciones',2,'../cotizaciones/ver_cotizaciones.php','find_in_page',2),(3,'Registrar siniestro',5,'../siniestros/registrar_siniestro.php','add',1),(4,'Ver siniestros',5,'../siniestros/ver_siniestros.php','find_in_page',2),(5,'Registrar actividad',5,'../siniestros/registrar_actividad.php','add',3),(6,'Ver actividades',5,'../siniestros/ver_actividades.php','fin_in_page',4),(7,'Permisos',7,'../catalogos/permisos.php','add',1),(8,'Empleados',7,'../catalogos/empleados.php','add',2),(9,'Aseguradoras',7,'../catalogos/aseguradoras.php','add',3),(10,'Clientes',7,'../catalogos/clientes.php','add',4),(11,'Contactos',7,'../catalogos/contactos.php','add',5),(12,'Registrar póliza',3,'../polizas/registrar_poliza.php','add',1),(13,'Ver pólizas',3,'../polizas/ver_polizas.php','add',2),(14,'Registrar pago',4,'../pagos/registrar_pago.php','add',1),(15,'Ver pagos',4,'../pagos/ver_pagos.php','add',2),(16,'Registrar póliza adjunta',3,'../polizas/registrar_poliza_adjunta.php','add',3),(17,'Ver pólizas adjuntas',3,'../polizas/ver_polizas_adjuntas.php','add',4),(18,'Indicadores',8,'../indicadores/indicadores.php','add',1),(20,'Bitácora',6,'../bitacora/bitacora.php','add',1);
/*!40000 ALTER TABLE `paginas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:26:30
