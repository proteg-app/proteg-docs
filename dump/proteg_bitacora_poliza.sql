CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bitacora_poliza`
--

DROP TABLE IF EXISTS `bitacora_poliza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bitacora_poliza` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_poliza` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  `descripcion` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitacora_poliza`
--

LOCK TABLES `bitacora_poliza` WRITE;
/*!40000 ALTER TABLE `bitacora_poliza` DISABLE KEYS */;
INSERT INTO `bitacora_poliza` VALUES (1,'2020-10-02 11:47:23',4,1,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: P1234.'),(2,'2020-10-02 13:31:41',6,2,1,'Jesús Ángel Saucedo Ruiz ha registrado una póliza con el folio: P4567.'),(3,'2020-11-05 14:15:16',2,3,1,'Marcela Salazar Sandoval ha registrado una póliza con el folio: prueba-poli.'),(4,'2020-11-05 14:25:19',2,4,1,'Marcela Salazar Sandoval ha registrado una póliza con el folio: prueba3.'),(5,'2020-12-21 02:59:13',4,6,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: test.'),(6,'2020-12-21 03:04:38',4,7,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 123412421.'),(7,'2020-12-21 03:09:37',4,8,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 142421.'),(8,'2020-12-21 03:18:21',4,9,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 421421421.'),(9,'2020-12-21 03:24:11',4,10,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: testetst.'),(10,'2020-12-27 22:17:31',4,11,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: gabrieltest.'),(11,'2020-12-27 22:30:24',4,12,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 421421.'),(12,'2020-12-27 23:27:02',4,13,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 123213rewrew.'),(13,'2020-12-27 23:27:34',4,14,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 44fdsgfd.'),(14,'2020-12-27 23:29:31',4,15,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 123abzcxv.'),(15,'2020-12-27 23:50:46',4,16,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 123abcabc.'),(16,'2020-12-27 23:55:09',4,17,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 421421421421fdsfdsdsa.'),(17,'2020-12-27 23:55:37',4,18,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 421421421421.'),(18,'2020-12-27 23:57:53',4,19,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 123abcasdasdadsa.'),(19,'2021-01-03 19:14:29',4,20,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: cxvczvc.'),(20,'2021-01-03 19:21:49',4,21,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 213vcxzcvcvxz.'),(21,'2021-01-04 05:31:19',4,23,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 231321321321321321.'),(22,'2021-01-12 01:33:58',4,24,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 213213.'),(23,'2021-05-27 20:19:03',4,25,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio: 255.'),(24,'2021-06-04 11:41:33',4,27,1,'Nallely Jaquelin Puente Carranza ha registrado una póliza con el folio:  GM0000098590.');
/*!40000 ALTER TABLE `bitacora_poliza` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:26:43
