CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contacto`
--

DROP TABLE IF EXISTS `contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) DEFAULT NULL,
  `tel` varchar(100) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `id_tipo_contacto` int(11) DEFAULT NULL,
  `url_logo` varchar(500) DEFAULT NULL,
  `orden_contacto` int(11) DEFAULT NULL,
  `tel_2` varchar(100) DEFAULT NULL,
  `tel_3` varchar(100) DEFAULT NULL,
  `tel_4` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacto`
--

LOCK TABLES `contacto` WRITE;
/*!40000 ALTER TABLE `contacto` DISABLE KEYS */;
INSERT INTO `contacto` VALUES (2,'JESÚS SAUCEDO','8442467350','j.saucedo@proteg.mx','Siniestros Autos/Daños',3,'https://www.proteg.mx/proteg/aseguradoras/logo.png',2,NULL,NULL,NULL),(3,'LILIANA AGUILLÓN','8442772164','lilianaaguillon@prodigy.net.mx','Siniestros GMM',3,'https://www.proteg.mx/proteg/aseguradoras/logo.png',3,NULL,NULL,NULL),(4,'MARCELA SALAZAR','8442781283','m.salazar@proteg.mx','Jefa de Oficina',3,'https://www.proteg.mx/proteg/aseguradoras/logo.png',4,NULL,NULL,NULL),(7,'POLICÍA','8441234563','','',2,'https://www.proteg.mx/proteg/aseguradoras/Ks4gVB6D.jpg',27,NULL,NULL,NULL),(8,'CRUZ ROJA','8444567894','','',2,'https://www.proteg.mx/proteg/aseguradoras/Flag_of_the_Red_Cross.png',25,'',NULL,NULL),(9,'BOMBEROS','8447894561','','',2,'https://www.proteg.mx/proteg/aseguradoras/GmRqcuNX_400x400.jpg',26,NULL,NULL,NULL),(10,'SEGUROS DE AFIRME S.A. DE C.V.','','','',4,'https://www.proteg.mx/proteg/aseguradoras/Personas-Seguros-MasSeguros.png',17,NULL,NULL,NULL),(11,'ANA COMPAÑÍA DE SEGUROS S.A. DE C.V.','','','',4,'https://www.proteg.mx/proteg/aseguradoras/ana-seguros-logo-450x250.png',20,'','',''),(12,'AXA SEGUROS S.A. DE C.V.','','','',1,'https://www.proteg.mx/proteg/aseguradoras/axa_logo_solid_rgb.png',7,NULL,NULL,NULL),(13,'SEGURO ATLAS S.A. ','','','',1,'https://www.proteg.mx/proteg/aseguradoras/ATLAS.png',13,NULL,NULL,NULL),(14,'GABRIEL DE LEÓN','8441627707','g.deleon@proteg.mx','Director',3,'https://www.proteg.mx/proteg/aseguradoras/logo.png',1,NULL,NULL,NULL),(15,'CHUBB SEGUROS MÉXICO S.A. DE C.V.','',NULL,NULL,4,'https://www.proteg.mx/proteg/aseguradoras/chubb-seguros-logo.jpg',18,NULL,NULL,NULL),(16,'GENERAL DE SEGUROS S.A. DE C.V.','','','',1,'https://www.proteg.mx/proteg/aseguradoras/GENERAL-DE-SEGUROS-LOGO.png',12,'','',NULL),(18,'GNP S.A. DE C.V.','','','',1,'https://www.proteg.mx/proteg/aseguradoras/logo-gnp.png',9,NULL,NULL,NULL),(19,'HDI SEGUROS S.A. DE C.V.','','','',4,'https://www.proteg.mx/proteg/aseguradoras/logohdi.png',15,'','',NULL),(20,'SEGUROS INBURSA S.A.','',NULL,NULL,1,'https://www.proteg.mx/proteg/aseguradoras/15862811310181.jpg',10,NULL,NULL,NULL),(21,'METLIFE S.A. DE C.V.','','',NULL,1,'https://www.proteg.mx/proteg/aseguradoras/Logo-MetLife.jpg',6,NULL,NULL,NULL),(22,'PRIMERO SEGUROS S.A. DE C.V.','','','',4,'https://www.proteg.mx/proteg/aseguradoras/PRIMEROSEGUROS.png',19,NULL,NULL,NULL),(23,'QUALITAS S.A. DE C.V.','','','',4,'https://www.proteg.mx/proteg/aseguradoras/Imagotipo_Banner_SitioWebQuálitas.png',14,'',NULL,NULL),(24,'SISNOVA S.A. DE C.V.',NULL,NULL,NULL,1,'https://www.proteg.mx/proteg/aseguradoras/logosis.png',11,NULL,NULL,NULL),(25,'SEGUROS MONTERREY NEW YORK LIFE',NULL,NULL,NULL,1,'https://www.proteg.mx/proteg/aseguradoras/logo_final.png',5,NULL,NULL,NULL),(26,'BRITISH UNITED PROVIDENT ASSOCIATION','',NULL,NULL,1,'https://www.proteg.mx/proteg/aseguradoras/1200px-Bupa_logo.svg.png',8,NULL,NULL,NULL),(27,'AXA SEGUROS S.A. DE C.V.',NULL,NULL,'1',4,'https://www.proteg.mx/proteg/aseguradoras/axa_logo_solid_rgb.png',16,NULL,NULL,NULL),(28,'GENERAL DE SEGUROS S.A. DE C.V.',NULL,NULL,NULL,4,'https://www.proteg.mx/proteg/aseguradoras/GENERAL-DE-SEGUROS-LOGO.png',21,NULL,NULL,NULL),(29,'SEGUROS INBURSA S.A.',NULL,NULL,NULL,4,'https://www.proteg.mx/proteg/aseguradoras/15862811310181.jpg',22,NULL,NULL,NULL),(30,'SEGURO ATLAS S.A. ',NULL,NULL,NULL,4,'https://www.proteg.mx/proteg/aseguradoras/ATLAS.png',23,NULL,NULL,NULL),(31,'GNP S.A. DE C.V.',NULL,NULL,NULL,4,'https://www.proteg.mx/proteg/aseguradoras/logo-gnp.png',24,NULL,NULL,NULL),(32,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `contacto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:26:42
