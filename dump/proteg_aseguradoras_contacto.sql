CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aseguradoras_contacto`
--

DROP TABLE IF EXISTS `aseguradoras_contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aseguradoras_contacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_contacto` int(11) DEFAULT NULL,
  `id_tipo_mensaje_contacto` int(11) DEFAULT NULL,
  `numero_contacto` varchar(200) DEFAULT NULL,
  `icono` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aseguradoras_contacto`
--

LOCK TABLES `aseguradoras_contacto` WRITE;
/*!40000 ALTER TABLE `aseguradoras_contacto` DISABLE KEYS */;
INSERT INTO `aseguradoras_contacto` VALUES (1,25,1,'018009062100','human'),(2,21,1,'01800006385433','human'),(3,12,1,'018009001292','human'),(4,26,1,'018886882872','human'),(5,18,1,'5552279000','human'),(6,20,1,'018009090000','human'),(7,24,1,'018000226436','human'),(8,16,1,'018004390500','human'),(9,13,1,'018008493918','human'),(10,23,1,'018008002021','human'),(11,19,1,'018000196000','human'),(12,27,1,'018009001292','human'),(13,10,1,'018007348294','human'),(14,15,1,'018007122828','human'),(15,22,1,'018002774637','human'),(16,11,1,'018008353262','human'),(17,28,1,'018004727696','human'),(18,29,1,'018009090000','human'),(19,30,1,'018008493917','human'),(20,31,1,'5552279000','human'),(21,8,1,'8444121206','human'),(22,9,1,'8447894561','human'),(23,7,1,'844 414 1616','human');
/*!40000 ALTER TABLE `aseguradoras_contacto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:26:16
