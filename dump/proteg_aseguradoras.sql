CREATE DATABASE  IF NOT EXISTS `proteg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proteg`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 198.12.248.204    Database: proteg
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aseguradoras`
--

DROP TABLE IF EXISTS `aseguradoras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aseguradoras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `sitio_web_pagos` varchar(200) DEFAULT NULL,
  `enlace_cotizar` varchar(250) DEFAULT NULL,
  `url_imagen` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aseguradoras`
--

LOCK TABLES `aseguradoras` WRITE;
/*!40000 ALTER TABLE `aseguradoras` DISABLE KEYS */;
INSERT INTO `aseguradoras` VALUES (1,'SEGUROS DE AFIRME S.A. DE C.V.','','','https://www.proteg.mx/proteg/aseguradoras/Personas-Seguros-MasSeguros.png'),(2,'ANA COMPAÑÍA DE SEGUROS S.A. DE C.V.','https://www.anaseguros.com.mx/anaweb/paga_tu_poliza.html','','https://www.proteg.mx/proteg/aseguradoras/ana-seguros-logo-450x250.png'),(3,'AXA SEGUROS S.A. DE C.V.','https://cajaaxa.mitec.com.mx/cua/inicio.do?method=loginAgente&perfil=cliente','','https://www.proteg.mx/proteg/aseguradoras/89070099_690253551596433_7494456471090888704_n.jpg'),(4,'SEGURO ATLAS S.A. ','https://hyperion.segurosatlas.com.mx/Portales/Pages/General/PagoLinea2.aspx','','https://www.proteg.mx/proteg/aseguradoras/ATLAS.png'),(5,'BRITISH UNITED PROVIDENT ASSOCIATION ','https://onlineservices.bupasalud.com/Bupa.OnlineServices/OnlineServicesWeb/policies/policy-payments-without-loggin?lang=SPA','','https://www.proteg.mx/proteg/aseguradoras/1200px-Bupa_logo.svg.png'),(6,'CHUBB SEGUROS MÉXICO S.A. DE C.V.','https://www.chubb.com/mx-es/linea-aba/','','https://www.proteg.mx/proteg/aseguradoras/chubb-seguros-logo.jpg'),(7,'GENERAL DE SEGUROS S.A. DE C.V.','','','https://www.proteg.mx/proteg/aseguradoras/GENERAL-DE-SEGUROS-LOGO.png'),(9,'GNP S.A. DE C.V.','https://www.gnp.com.mx/soy-cliente#!/login','','https://www.proteg.mx/proteg/aseguradoras/76953563_10162821776420650_3472569977523929088_o.jpg'),(10,'HDI SEGUROS S.A. DE C.V.','https://www.hdi.com.mx/pago-de-polizas-2/ ','','https://www.proteg.mx/proteg/aseguradoras/logohdi.png'),(11,'SEGUROS INBURSA S.A.','https://www.inbursa.com/Portal/?page=Document/doc_view_section.asp&id_document=1036&id_category=48','','https://www.proteg.mx/proteg/aseguradoras/15862811310181.jpg'),(12,'METLIFE S.A. DE C.V.','https://servicios.metlife.com.mx/MSI/cliente','','https://www.proteg.mx/proteg/aseguradoras/Logo-MetLife.jpg'),(13,'PRIMERO SEGUROS S.A. DE C.V.','','','https://www.proteg.mx/proteg/aseguradoras/PRIMEROSEGUROS.png'),(14,'QUALITAS S.A. DE C.V.','https://www.qualitas.com.mx/web/qmx/pago-de-poliza ','','https://www.proteg.mx/proteg/aseguradoras/Imagotipo_Banner_SitioWebQuálitas.png'),(15,'SISNOVA S.A. DE C.V.','','','https://www.proteg.mx/proteg/aseguradoras/logosis.png'),(16,'SMNYL SEGUROS MONTERREY NEW YORK LIFE','https://www.smnyl-clientes.com.mx/pago-en-linea/',NULL,'https://www.proteg.mx/proteg/aseguradoras/logo_final.png');
/*!40000 ALTER TABLE `aseguradoras` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-04 13:26:48
